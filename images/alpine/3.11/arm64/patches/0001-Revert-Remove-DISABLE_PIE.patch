From 58ada9287d4e70b85dce9a3f16f0312b16f47001 Mon Sep 17 00:00:00 2001
From: Eric Timmons <etimmons@mit.edu>
Date: Sun, 27 Sep 2020 20:52:43 -0400
Subject: [PATCH] Revert "Remove DISABLE_PIE"

This reverts commit 78de8730813a8a2d0c7f5c125fff3cfaa129f63b.
---
 src/runtime/Config.generic-openbsd |  9 +++++++++
 src/runtime/Config.riscv-linux     |  1 +
 src/runtime/Config.sparc-sunos     |  4 ++++
 src/runtime/Config.x86-64-darwin   |  1 +
 src/runtime/Config.x86-64-haiku    |  5 +++++
 src/runtime/Config.x86-64-linux    |  1 +
 src/runtime/Config.x86-64-openbsd  |  1 +
 src/runtime/GNUmakefile            | 22 ++++++++++++++++++++++
 8 files changed, 44 insertions(+)

diff --git a/src/runtime/Config.generic-openbsd b/src/runtime/Config.generic-openbsd
index f4a393b0c..6936223e5 100644
--- a/src/runtime/Config.generic-openbsd
+++ b/src/runtime/Config.generic-openbsd
@@ -16,3 +16,12 @@ ifdef LISP_FEATURE_SB_THREAD
 CFLAGS += -pthread
 OS_LIBS += -pthread
 endif
+
+ifeq ($(DISABLE_PIE),yes)
+ifneq ($(shell $(CC) -dM -E - < /dev/null 2>/dev/null | grep -e '__clang__'),)
+CFLAGS += -fno-pie
+LINKFLAGS += -nopie
+LDFLAGS += -nopie
+__LDFLAGS__ += -nopie
+endif
+endif
diff --git a/src/runtime/Config.riscv-linux b/src/runtime/Config.riscv-linux
index 3e49bc2ee..5d585934e 100644
--- a/src/runtime/Config.riscv-linux
+++ b/src/runtime/Config.riscv-linux
@@ -22,6 +22,7 @@ ifdef LISP_FEATURE_SB_CORE_COMPRESSION
   OS_LIBS += -lz
 endif
 LINKFLAGS += -Wl,--export-dynamic
+DISABLE_PIE=no
 
 ifdef LISP_FEATURE_LARGEFILE
   CFLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64
diff --git a/src/runtime/Config.sparc-sunos b/src/runtime/Config.sparc-sunos
index 897b5c4d7..911b088cd 100644
--- a/src/runtime/Config.sparc-sunos
+++ b/src/runtime/Config.sparc-sunos
@@ -14,6 +14,10 @@ CFLAGS += -DSVR4 -D_REENTRANT
 ASFLAGS = -g -DSVR4 -Wa,-xarch=v8plus
 #LINKFLAGS += -v
 NM = nm -t x -p 
+# This next line has nothing to do with disabling PIE. It has only to
+# do with the problem that "grep" on the build machine I'm using can't
+# parse "-e '[^f]nopie" and so gets an error in GNUmakefile.
+DISABLE_PIE=no
 
 ASSEM_SRC = sparc-assem.S
 ARCH_SRC = sparc-arch.c
diff --git a/src/runtime/Config.x86-64-darwin b/src/runtime/Config.x86-64-darwin
index fb466f1ad..6e6775c7c 100644
--- a/src/runtime/Config.x86-64-darwin
+++ b/src/runtime/Config.x86-64-darwin
@@ -42,6 +42,7 @@ ARCH_SRC = x86-64-arch.c
 LINKFLAGS += -arch x86_64 -dynamic -twolevel_namespace -bind_at_load -pagezero_size 0x100000
 
 CFLAGS += -arch x86_64 -fno-omit-frame-pointer
+DISABLE_PIE=no
 
 ifdef LISP_FEATURE_IMMOBILE_SPACE
   GC_SRC = fullcgc.c gencgc.c traceroot.c immobile-space.c
diff --git a/src/runtime/Config.x86-64-haiku b/src/runtime/Config.x86-64-haiku
index 154f720b1..643cbb872 100644
--- a/src/runtime/Config.x86-64-haiku
+++ b/src/runtime/Config.x86-64-haiku
@@ -21,6 +21,11 @@ ifdef LISP_FEATURE_SB_THREAD
 endif
 
 CFLAGS += -Wunused-parameter -fno-omit-frame-pointer -momit-leaf-frame-pointer -gdwarf-2
+# The installed compiler won't work with no-pie.
+# The support ticket https://dev.haiku-os.org/ticket/12430 is closed,
+# but it certainly looks like my default install succumbed to the same problem
+# in the gcc specs file in the exact way as illustrated in the bug.
+DISABLE_PIE=no
 
 ifdef LISP_FEATURE_IMMOBILE_SPACE
   GC_SRC = fullcgc.c gencgc.c traceroot.c immobile-space.c elf.c
diff --git a/src/runtime/Config.x86-64-linux b/src/runtime/Config.x86-64-linux
index a638b9ede..a9dc7f008 100644
--- a/src/runtime/Config.x86-64-linux
+++ b/src/runtime/Config.x86-64-linux
@@ -47,6 +47,7 @@ ifdef LISP_FEATURE_LIBUNWIND_BACKTRACE
 endif
 
 CFLAGS += -Wunused-parameter -Wimplicit-fallthrough -fno-omit-frame-pointer -momit-leaf-frame-pointer
+DISABLE_PIE=no
 
 ifdef LISP_FEATURE_IMMOBILE_SPACE
   GC_SRC = fullcgc.c gencgc.c traceroot.c immobile-space.c elf.c
diff --git a/src/runtime/Config.x86-64-openbsd b/src/runtime/Config.x86-64-openbsd
index 333fdf224..bc95d81ff 100644
--- a/src/runtime/Config.x86-64-openbsd
+++ b/src/runtime/Config.x86-64-openbsd
@@ -10,4 +10,5 @@
 # files for more information.
 
 include Config.x86-64-bsd
+DISABLE_PIE=no
 include Config.generic-openbsd
diff --git a/src/runtime/GNUmakefile b/src/runtime/GNUmakefile
index 281fe1bd5..8e3525d00 100644
--- a/src/runtime/GNUmakefile
+++ b/src/runtime/GNUmakefile
@@ -47,8 +47,30 @@ include genesis/Makefile.features
 # symlink to Config-foo.
 # Commonly used variables in Config are: ARCH_SRC, ASSEM_SRC, GC_SRC,
 # OS_SRC, OS_LIBS, OS_CLEAN_FILES
+DISABLE_PIE=yes
 include Config
 
+# Disable PIE by default.
+# We mostly do not care any more where the C code is located, and would
+# prefer that C toolchain default be used, however, the limited address
+# space in 32-bit architectures make it tricky to permit the .text segment
+# to be placed arbitrarily if there is any risk of not being able to
+# allocate the lisp readonly and static spaces on account of collision.
+ifeq ($(DISABLE_PIE),yes)
+ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]no-pie'),)
+CFLAGS += -fno-pie
+LINKFLAGS += -no-pie
+LDFLAGS += -no-pie
+__LDFLAGS__ += -no-pie
+endif
+ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]nopie'),)
+CFLAGS += -fno-pie
+LINKFLAGS += -nopie
+LDFLAGS += -nopie
+__LDFLAGS__ += -nopie
+endif
+endif
+
 COMMON_SRC = alloc.c backtrace.c breakpoint.c coalesce.c coreparse.c    \
 	dynbind.c funcall.c gc-common.c globals.c hopscotch.c           \
 	interr.c interrupt.c largefile.c main.c                         \
-- 
2.28.0

